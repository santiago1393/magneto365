const axios = require('axios');
require('dotenv').config();
const api = process.env.API;


const get_movies = () => {
    return new Promise((resolve, reject) => {
        axios.get(`${api}films`).then((result) => {
            resolve(result.data);
        }).catch((err) => {
            reject(err);
        });
    });
};

const get_planets = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url).then((result) => {
            const planet_data = {
                name: result.data.name,
                terrain: result.data.terrain,
                gravity: result.data.gravity,
                diameter: result.data.diameter,
                population: result.data.population,
            };
            resolve(planet_data);
        }).catch((err) => {
            reject(err);
        });
    });
};

const get_actors = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url).then((result) => {
            const actor_data = {
                name: result.data.name,
                gender: result.data.gender,
                hair_color: result.data.hair_color,
                skin_color: result.data.skin_color,
                eye_color: result.data.eye_color,
                height: result.data.height,
                homeworld: result.data.homeworld,
                species: result.data.species,
            };
            resolve(actor_data);
        }).catch((err) => {
            reject(err);
        });
    });
};

const get_starships = (url) => {
    return new Promise((resolve, reject) => {
        axios.get(url).then((result) => {
            const starship_data = {
                name: result.data.name,
                model: result.data.model,
                manufacturer: result.data.manufacturer,
                passengers: result.data.passengers,
            };
            resolve(starship_data);
        }).catch((err) => {
            reject(err);
        });
    });
};

const get_movie_data = (movie) => {
    return new Promise((resolve, reject) => {

        try {
            let movie_data = {
                name: "",
                planets: [],
                characters: [],
                starships: []
            };

            movie_data.name = movie.title;

            let planet_promise_array = [];

            movie.planets.forEach((planet) => {
                planet_promise_array.push(get_planets(planet));
            });

            let characters_promise_array = [];

            movie.characters.forEach((character) => {
                characters_promise_array.push(get_actors(character));
            });

            let startships_promise_array = [];

            movie.starships.forEach((starship) => {
                startships_promise_array.push(get_starships(starship));
            });

            let planets = Promise.all(planet_promise_array).then((val) => {
                movie_data.planets = val;
            });

            let starships = Promise.all(startships_promise_array).then((val) => {
                movie_data.starships = val;
            });

            let characters = Promise.all(characters_promise_array).then((val) => {
                movie_data.characters = val;
            });

            Promise.all([planets, starships, characters]).then((val) => {
                resolve(movie_data);
            });

        } catch (error) {
            reject(error);
        }
    });
};

const get_data = () => {
    return new Promise((resolve, reject) => {

        get_movies().then(result => {

            let movies = [];
            let movies_promises = [];

            result.results.forEach((movie) => {
                const movie_promise = get_movie_data(movie);
                movies_promises.push(movie_promise);
            });

            Promise.all(movies_promises).then((val) => {
                movies = val;
                resolve(movies);
            });
        });
    });
};

get_data().then((val) => {
    console.log(JSON.stringify(val));
});